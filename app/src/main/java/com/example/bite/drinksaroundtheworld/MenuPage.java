package com.example.bite.drinksaroundtheworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Florin on 11/7/2015.
 */
public class MenuPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menupage);


        FloatingActionButton returnButton = (FloatingActionButton) findViewById(R.id.to_main);
        returnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuPage.this, MapsActivity.class);
                startActivity(intent);
            }
        });
    }
}